conda activate kaggle

rm -rf output/*


export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
kaggle kernels output remotejob/mlgtp2createmodel -p output/


unset KAGGLE_CONFIG_DIR
kaggle kernels output alesandermazurov/mlgtp2createmodel -p output/

mv  output/best_model/*  data/models
