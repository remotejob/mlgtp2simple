import os
file_name = "/kaggle/input/filteredtwdata/filteredtw.csv"

os.system('pip install --no-cache-dir -U tensorflow-gpu==1.15.0')

os.system('pip install --no-cache-dir -U gpt-2-finetuning==0.10')
os.system('download_gpt2_model 355M')
MODEL = '355M'

import os
import tensorflow as tf
import numpy as np

from gpt_2_finetuning.interactive_conditional_samples import interact_model
from gpt_2_finetuning.train import train

train(dataset_path=file_name,
      model_name=MODEL,
      n_steps=10000,
      save_every=5000,
      sample_every=1000,
      mem_saving_gradients=True,
      print_loss_every=1000,
      max_checkpoints_to_keep=2)



# os.system('pip install --no-cache-dir -U tensorflow-gpu==1.15.0')
# os.system('pip install --no-cache-dir -U gpt_2_simple')

# import gpt_2_simple as gpt2
# import os
# # import requests

# model_name = "124M"
# #model_name = "355M"
# if not os.path.isdir(os.path.join("models", model_name)):
# 	print(f"Downloading {model_name} model...")
# 	gpt2.download_gpt2(model_name=model_name)   # model is saved into current directory under /models/124M/


# file_name = "/kaggle/input/filteredtwdata/filteredtw.csv"
# # if not os.path.isfile(file_name):
# # 	url = "https://raw.githubusercontent.com/karpathy/char-rnn/master/data/tinyshakespeare/input.txt"
# # 	data = requests.get(url)
	
# # 	with open(file_name, 'w') as f:
# # 		f.write(data.text)
    

# sess = gpt2.start_tf_sess()
# gpt2.finetune(sess,
#             #   file_name,model_dir='/kaggle/input/openai-gpt2-weights/355M',
#               file_name,
#               model_name=model_name,
#               steps=10) # steps is max number of training steps

# gpt2.generate(sess)