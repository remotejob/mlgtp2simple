
# # Fine-tuning GPT-2 on a jokes dataset in PyTorch
# 
# This notebook was created as a part of a blog post - [Fine-tuning large Transformer models on a single GPU in PyTorch - Teaching GPT-2 a 
# sense of humor](https://mf1024.github.io/2019/11/12/Fun-With-GPT-2/). Here I demonstrate how to fine-tune a pre-trained GPT-2 model on a jokes dataset. 
# 
# Let's see if the model can learn to crack some jokes!
# 
# For this experiment, I will use a pre-trained GPT-2 medium-sized model from the huggingface [transformers repository](https://github.com/huggingface/transformers).
# 
# #### If you haven't yet, check out the notebook in this [gist](https://gist.github.com/mf1024/430d7fd6ff527350d3e4b5bda0d8614e) where use the same pretrained model to generate text.

import os

os.system('pip install -U wandb')

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb
wandb.init(project="gpt2model")

import torch
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import numpy as np

import logging
logging.getLogger().setLevel(logging.CRITICAL)

import warnings
warnings.filterwarnings('ignore')

device = 'cpu'
if torch.cuda.is_available():
    device = 'cuda'


# tokenizer = GPT2Tokenizer.from_pretrained('gpt2-medium')
# model = GPT2LMHeadModel.from_pretrained('gpt2-medium')
tokenizer = GPT2Tokenizer.from_pretrained('/kaggle/input/gpt2medium-pytorch/gpt2_medium')
model = GPT2LMHeadModel.from_pretrained('/kaggle/input/gpt2medium-pytorch/gpt2_medium')
model = model.to(device)

def choose_from_top(probs, n=5):
    ind = np.argpartition(probs, -n)[-n:]
    top_prob = probs[ind]
    top_prob = top_prob / np.sum(top_prob) # Normalize
    choice = np.random.choice(n, 1, p = top_prob)
    token_id = ind[choice][0]
    return int(token_id)

from torch.utils.data import Dataset
from torch.utils.data import Dataset, DataLoader
import os
import json
import csv

class JokesDataset(Dataset):
    def __init__(self, jokes_dataset_path = '/kaggle/input/filteredtwdata/'):
        super().__init__()

        short_jokes_path = os.path.join(jokes_dataset_path, 'val.csv')

        self.joke_list = []
        self.end_of_text_token = "<|endoftext|>"
        
        with open(short_jokes_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            
            x = 0
            for row in csv_reader:
                # joke_str = f"Hei:{row[0]}{self.end_of_text_token}"
                joke_str = row[0]+self.end_of_text_token
                self.joke_list.append(joke_str)
        
    def __len__(self):
        return len(self.joke_list)

    def __getitem__(self, item):
        return self.joke_list[item]

dataset = JokesDataset()
joke_loader = DataLoader(dataset, batch_size=1, shuffle=True)

BATCH_SIZE = 128
EPOCHS = 2
LEARNING_RATE = 1e-3
WARMUP_STEPS = 5000
MAX_SEQ_LEN = 400
from transformers import AdamW,get_linear_schedule_with_warmup

device = 'cpu'
if torch.cuda.is_available():
    device = 'cuda'

model = model.to(device)
model.train()
optimizer = AdamW(model.parameters(), lr=LEARNING_RATE,eps = 1e-5)

total_steps = len(joke_loader) * EPOCHS

scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                num_warmup_steps = 500, # Default value in run_glue.py
                                                num_training_steps = total_steps)
proc_seq_count = 0
sum_loss = 0.0
batch_count = 0

tmp_jokes_tens = None
models_folder = "trained_models"
if not os.path.exists(models_folder):
    os.mkdir(models_folder)

for epoch in range(EPOCHS):
    
    print(f"EPOCH {epoch} started" + '=' * 30)
    
    for idx,joke in enumerate(joke_loader):
        
        #################### "Fit as many joke sequences into MAX_SEQ_LEN sequence as possible" logic start ####
        joke_tens = torch.tensor(tokenizer.encode(joke[0])).unsqueeze(0).to(device)
        #Skip sample from dataset if it is longer than MAX_SEQ_LEN
        if joke_tens.size()[1] > MAX_SEQ_LEN:
            continue
        
        #The first joke sequence in the sequence
        if not torch.is_tensor(tmp_jokes_tens):
            tmp_jokes_tens = joke_tens
            continue
        else:
            #The next joke does not fit in so we process the sequence and leave the last joke 
            #as the start for next sequence 
            if tmp_jokes_tens.size()[1] + joke_tens.size()[1] > MAX_SEQ_LEN:
                work_jokes_tens = tmp_jokes_tens
                tmp_jokes_tens = joke_tens
            else:
                #Add the joke to sequence, continue and try to add more
                tmp_jokes_tens = torch.cat([tmp_jokes_tens, joke_tens[:,1:]], dim=1)
                continue
        ################## Sequence ready, process it trough the model ##################
            
        outputs = model(work_jokes_tens, labels=work_jokes_tens)
        loss, logits = outputs[:2]
                             
        loss.backward()
        sum_loss = sum_loss + loss.detach().data
                       
        # proc_seq_count = proc_seq_count + 1
        # if proc_seq_count == BATCH_SIZE:
            # proc_seq_count = 0    
            # batch_count += 1
        optimizer.step()
        lr = scheduler.get_last_lr()[0]
        wandb.log({'lr':lr})
        scheduler.step() 
        optimizer.zero_grad()
        model.zero_grad()

        if idx % 10 == 0 and not idx == 0:
            # wandb.log({"train_loss": loss.item()})
            wandb.log({"train_loss": sum_loss/10})
            print(f"sum loss {sum_loss}")
            sum_loss = 0.0

        # if batch_count == 50:
        #     print(f"sum loss {sum_loss}")
        #     batch_count = 0
        #     sum_loss = 0.0
    
    # Store the model after each epoch to compare the performance of them
    if epoch == 1:
        torch.save(model.state_dict(), os.path.join(models_folder, f"gpt2_medium_joker_{epoch}.pt"))

MODEL_EPOCH = 1

models_folder = "trained_models"

model_path = os.path.join(models_folder, f"gpt2_medium_joker_{MODEL_EPOCH}.pt")
model.load_state_dict(torch.load(model_path))

jokes_output_file_path = f'generated_{MODEL_EPOCH}.jokes'

model.eval()
if os.path.exists(jokes_output_file_path):
    os.remove(jokes_output_file_path)
    
joke_num = 0
with torch.no_grad():
   
        for joke_idx in range(1000):
        
            joke_finished = False

            cur_ids = torch.tensor(tokenizer.encode("Hei:")).unsqueeze(0).to(device)

            for i in range(100):
                outputs = model(cur_ids, labels=cur_ids)
                loss, logits = outputs[:2]
                softmax_logits = torch.softmax(logits[0,-1], dim=0) #Take the first(from only one in this case) batch and the last predicted embedding
                if i < 3:
                    n = 20
                else:
                    n = 3
                next_token_id = choose_from_top(softmax_logits.to('cpu').numpy(), n=n) #Randomly(from the topN probability distribution) select the next word
                cur_ids = torch.cat([cur_ids, torch.ones((1,1)).long().to(device) * next_token_id], dim = 1) # Add the last word to the running sequence

                if next_token_id in tokenizer.encode('<|endoftext|>'):
                    joke_finished = True
                    break

            
            if joke_finished:
                
                joke_num = joke_num + 1
                
                output_list = list(cur_ids.squeeze().to('cpu').numpy())
                output_text = tokenizer.decode(output_list)

                with open(jokes_output_file_path, 'a') as f:
                    f.write(f"{output_text} \n\n")

