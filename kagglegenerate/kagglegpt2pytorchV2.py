
# https://towardsdatascience.com/teaching-gpt-2-a-sense-of-humor-fine-tuning-large-transformer-models-on-a-single-gpu-in-pytorch-59e8cec40912
import os

# os.system('pip install -U wandb')

# os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
# import wandb
# wandb.init(project="gpt2model")

import torch
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import numpy as np
import pandas as pd
import random

import logging
logging.getLogger().setLevel(logging.CRITICAL)

import warnings
warnings.filterwarnings('ignore')

# device = 'cpu'
# if torch.cuda.is_available():
#     device = 'cuda'

datapath ='/kaggle/input/filteredtwdata/'

best_model_path ='/kaggle/working/best_model'
os.mkdir(best_model_path)

df = pd.read_csv(datapath +'val.csv', error_bad_lines=False,header=None,names=['sentence'])


# print(df.sample(20))

tokenizer = GPT2Tokenizer.from_pretrained('/kaggle/input/gpt2medium-pytorch/gpt2_medium')

tokenizer.pad_token = tokenizer.eos_token


sentences = df.sentence.values

max_len = 223

# for sent in sentences:

#     # Tokenize the text and add `[CLS]` and `[SEP]` tokens.
#     input_ids = tokenizer.encode(sent, add_special_tokens=True)

#     # Update the maximum sentence length.
#     max_len = max(max_len, len(input_ids))

# print('Max sentence length: ', max_len)


# encoded_dict0 = tokenizer.encode_plus(text="Hello I am Moin", add_special_tokens=True, \
#     max_length=223, truncation_strategy="longest_first", pad_to_max_length=False, \
#     return_tensors=None, return_token_type_ids=True, return_attention_mask=True, \
#     return_overflowing_tokens=False, return_special_tokens_mask=False)

# print("bos",tokenizer.bos_token_id)
# print("dic0",encoded_dict0['input_ids'])
def choose_from_top(probs, n=5):
    ind = np.argpartition(probs, -n)[-n:]
    top_prob = probs[ind]
    top_prob = top_prob / np.sum(top_prob) # Normalize
    choice = np.random.choice(n, 1, p = top_prob)
    token_id = ind[choice][0]
    return int(token_id)

input_ids = []
attention_masks = []


for sent in sentences:

 
    encoded_dict = tokenizer.encode_plus(
                        text=sent,                      # Sentence to encode.
                        add_special_tokens = True, # Add '[CLS]' and '[SEP]'
                        max_length = max_len,
                        truncation_strategy="longest_first",           
                        pad_to_max_length = True,
                        return_attention_mask = True,   # Construct attn. masks.
                        return_tensors = 'pt',     # Return pytorch tensors.
                        return_overflowing_tokens=False, 
                        return_special_tokens_mask=False
                   )
        
    # Add the encoded sentence to the list.    
    input_ids.append(encoded_dict['input_ids'])
    attention_masks.append(encoded_dict['attention_mask'])
    
    # And its attention mask (simply differentiates padding from non-padding).
    # attention_masks.append(encoded_dict['attention_mask'])

# for i in range(5):
#     print(type(input_ids[i]))
#     print("dic",input_ids[i])
# # print(len(input_ids))

# # Convert the lists into tensors.
input_ids = torch.cat(input_ids, dim=0)
attention_masks = torch.cat(attention_masks, dim=0)
# # attention_masks = torch.cat(attention_masks, dim=0)
# # labels = torch.tensor(labels)

# # Print sentence 0, now as a list of IDs.
# print('Original: ', sentences[0])
# print('Token IDs:', input_ids[0])
# print('attention_mask',attention_masks[0])

from torch.utils.data import TensorDataset

dataset = TensorDataset(input_ids, attention_masks, input_ids)

from torch.utils.data import DataLoader, RandomSampler, SequentialSampler


def ret_dataloader():
    train_dataloader = DataLoader(
                    dataset,  # The training samples.
                    sampler = RandomSampler(dataset), # Select batches randomly
                    batch_size = 2 # Trains with this batch size.
                )
    return train_dataloader

from transformers import AdamW, BertConfig
   

def ret_model():
    model = GPT2LMHeadModel.from_pretrained('/kaggle/input/gpt2medium-pytorch/gpt2_medium')

    return model


def ret_optim(model):
    # print('Learning_rate = ',wandb.config.learning_rate )
    # print('Learning_rate = ',5e-5)
    optimizer = AdamW(model.parameters(),
                    
                    lr = 0.0001, 
                    eps = 1e-8 
                    )
    return optimizer


from transformers import get_linear_schedule_with_warmup

def ret_scheduler(train_dataloader,optimizer):
    # epochs = wandb.config.epochs
    # print('epochs =>', epochs)
    # Total number of training steps is [number of batches] x [number of epochs]. 
    # (Note that this is not the same as the number of training samples).
    total_steps = len(train_dataloader) * 2

    # Create the learning rate scheduler.
    scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                num_warmup_steps = 7, # Default value in run_glue.py
                                                num_training_steps = total_steps)
    return scheduler


def train():
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = ret_model()
    model.to(device)
    train_dataloader = ret_dataloader()

    optimizer = ret_optim(model)
    scheduler = ret_scheduler(train_dataloader,optimizer)
    seed_val = 42
   
    random.seed(seed_val)
    np.random.seed(seed_val)
    torch.manual_seed(seed_val)

    model.train()
    for epoch_i in range(0, 2):

        for step, batch in enumerate(train_dataloader):
            # print(batch)
            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)  
            outputs = model(b_input_ids, 
                    labels=b_input_ids)
            loss, logits = outputs[:2]       
            print(loss.item())
            loss.backward()
            # torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

            optimizer.step()
            scheduler.step()
    
       
    # model.save_pretrained(best_model_path)
    torch.save(model.state_dict(), os.path.join(best_model_path, "model.pt"))

    model.load_state_dict(torch.load(os.path.join(best_model_path, "model.pt")))

    jokes_output_file_path ='generated_.jokes'

    model.eval()
    with torch.no_grad():
        for joke_idx in range(1000):
        
            joke_finished = False

            cur_ids = torch.tensor(tokenizer.encode("Hei:")).unsqueeze(0).to(device)

            for i in range(100):
                outputs = model(cur_ids, labels=cur_ids)
                loss, logits = outputs[:2]
                softmax_logits = torch.softmax(logits[0,-1], dim=0) #Take the first(from only one in this case) batch and the last predicted embedding
                if i < 3:
                    n = 20
                else:
                    n = 3
                next_token_id = choose_from_top(softmax_logits.to('cpu').numpy(), n=n) #Randomly(from the topN probability distribution) select the next word
                cur_ids = torch.cat([cur_ids, torch.ones((1,1)).long().to(device) * next_token_id], dim = 1) # Add the last word to the running sequence

                if next_token_id in tokenizer.encode('<|endoftext|>'):
                    joke_finished = True
                    break

            
            if joke_finished:
                
                # joke_num = joke_num + 1
                
                output_list = list(cur_ids.squeeze().to('cpu').numpy())
                output_text = tokenizer.decode(output_list)

                with open(jokes_output_file_path, 'a') as f:
                    f.write(f"{output_text} \n\n")




train()

# tokenizer = GPT2Tokenizer.from_pretrained('gpt2-medium')
# model = GPT2LMHeadModel.from_pretrained('gpt2-medium')
# tokenizer = GPT2Tokenizer.from_pretrained('/kaggle/input/gpt2medium-pytorch/gpt2_medium')
# model = GPT2LMHeadModel.from_pretrained('/kaggle/input/gpt2medium-pytorch/gpt2_medium')
# model = model.to(device)

# def choose_from_top(probs, n=5):
#     ind = np.argpartition(probs, -n)[-n:]
#     top_prob = probs[ind]
#     top_prob = top_prob / np.sum(top_prob) # Normalize
#     choice = np.random.choice(n, 1, p = top_prob)
#     token_id = ind[choice][0]
#     return int(token_id)

# from torch.utils.data import Dataset
# from torch.utils.data import Dataset, DataLoader
# import os
# import json
# import csv

# class JokesDataset(Dataset):
#     def __init__(self, jokes_dataset_path = '/kaggle/input/filteredtwdata/'):
#         super().__init__()

#         short_jokes_path = os.path.join(jokes_dataset_path, 'val.csv')

#         self.joke_list = []
#         self.end_of_text_token = "<|endoftext|>"
        
#         with open(short_jokes_path) as csv_file:
#             csv_reader = csv.reader(csv_file, delimiter=',')
            
#             x = 0
#             for row in csv_reader:
#                 # joke_str = f"Hei:{row[0]}{self.end_of_text_token}"
#                 joke_str = row[0]+self.end_of_text_token
#                 self.joke_list.append(joke_str)
        
#     def __len__(self):
#         return len(self.joke_list)

#     def __getitem__(self, item):
#         return self.joke_list[item]

# dataset = JokesDataset()
# joke_loader = DataLoader(dataset, batch_size=1, shuffle=True)

# BATCH_SIZE = 128
# EPOCHS = 1
# LEARNING_RATE = 1e-3
# WARMUP_STEPS = 5000
# MAX_SEQ_LEN = 500
# from transformers import AdamW,get_linear_schedule_with_warmup

# device = 'cpu'
# if torch.cuda.is_available():
#     device = 'cuda'

# model = model.to(device)
# model.train()
# optimizer = AdamW(model.parameters(), lr=LEARNING_RATE,eps = 1e-5)

# total_steps = len(joke_loader) * EPOCHS

# scheduler = get_linear_schedule_with_warmup(optimizer, 
#                                                 num_warmup_steps = 500, # Default value in run_glue.py
#                                                 num_training_steps = total_steps)
# proc_seq_count = 0
# sum_loss = 0.0
# batch_count = 0

# tmp_jokes_tens = None
# models_folder = "trained_models"
# if not os.path.exists(models_folder):
#     os.mkdir(models_folder)

# for epoch in range(EPOCHS):
    
#     print(f"EPOCH {epoch} started" + '=' * 30)
    
#     for idx,joke in enumerate(joke_loader):

#         # print(idx,joke)
#         print(idx,joke[0])
        
#         #################### "Fit as many joke sequences into MAX_SEQ_LEN sequence as possible" logic start ####
#         joke_tens = torch.tensor(tokenizer.encode(joke[0])).unsqueeze(0).to(device)

#         print(joke_tens.shape[1])


#         #Skip sample from dataset if it is longer than MAX_SEQ_LEN

#         if joke_tens.size()[1] > MAX_SEQ_LEN:
#             print("skip",joke_tens.size()[1])
#             continue
        
#         else:
#             print("NOT skip",joke_tens.size()[1])
#         # #The first joke sequence in the sequence
#         if not torch.is_tensor(tmp_jokes_tens):
#             tmp_jokes_tens = joke_tens
#             continue
#         else:
#             #The next joke does not fit in so we process the sequence and leave the last joke 
#             #as the start for next sequence 
#             if tmp_jokes_tens.size()[1] + joke_tens.size()[1] > MAX_SEQ_LEN:
#                 work_jokes_tens = tmp_jokes_tens
#                 tmp_jokes_tens = joke_tens
#             else:
#                 #Add the joke to sequence, continue and try to add more
#                 tmp_jokes_tens = torch.cat([tmp_jokes_tens, joke_tens[:,1:]], dim=1)
#                 continue

#         print("WORK",work_jokes_tens.shape)    
        ################## Sequence ready, process it trough the model ##################
            
        # outputs = model(work_jokes_tens, labels=work_jokes_tens)
        # loss, logits = outputs[:2]
                             
        # loss.backward()
        # sum_loss = sum_loss + loss.detach().data
                       
        # # proc_seq_count = proc_seq_count + 1
        # # if proc_seq_count == BATCH_SIZE:
        #     # proc_seq_count = 0    
        #     # batch_count += 1
        # optimizer.step()
        # lr = scheduler.get_last_lr()[0]
        # # wandb.log({'lr':lr})
        # scheduler.step() 
        # optimizer.zero_grad()
        # model.zero_grad()

        # if idx % 10 == 0 and not idx == 0:
        #     # wandb.log({"train_loss": loss.item()})
        #     # wandb.log({"train_loss": sum_loss/10})
        #     print(f"sum loss {sum_loss}")
        #     sum_loss = 0.0

        # if batch_count == 50:
        #     print(f"sum loss {sum_loss}")
        #     batch_count = 0
        #     sum_loss = 0.0
    
    # Store the model after each epoch to compare the performance of them
    # if epoch == 119:
    #     torch.save(model.state_dict(), os.path.join(models_folder, f"gpt2_medium_joker_{epoch}.pt"))

# MODEL_EPOCH = 119

# models_folder = "trained_models"

# model_path = os.path.join(models_folder, f"gpt2_medium_joker_{MODEL_EPOCH}.pt")
# model.load_state_dict(torch.load(model_path))

# jokes_output_file_path = f'generated_{MODEL_EPOCH}.jokes'

# model.eval()
# if os.path.exists(jokes_output_file_path):
#     os.remove(jokes_output_file_path)
    
# joke_num = 0
# with torch.no_grad():
   
#         for joke_idx in range(1000):
        
#             joke_finished = False

#             cur_ids = torch.tensor(tokenizer.encode("Hei:")).unsqueeze(0).to(device)

#             for i in range(100):
#                 outputs = model(cur_ids, labels=cur_ids)
#                 loss, logits = outputs[:2]
#                 softmax_logits = torch.softmax(logits[0,-1], dim=0) #Take the first(from only one in this case) batch and the last predicted embedding
#                 if i < 3:
#                     n = 20
#                 else:
#                     n = 3
#                 next_token_id = choose_from_top(softmax_logits.to('cpu').numpy(), n=n) #Randomly(from the topN probability distribution) select the next word
#                 cur_ids = torch.cat([cur_ids, torch.ones((1,1)).long().to(device) * next_token_id], dim = 1) # Add the last word to the running sequence

#                 if next_token_id in tokenizer.encode('<|endoftext|>'):
#                     joke_finished = True
#                     break

            
#             if joke_finished:
                
#                 joke_num = joke_num + 1
                
#                 output_list = list(cur_ids.squeeze().to('cpu').numpy())
#                 output_text = tokenizer.decode(output_list)

#                 with open(jokes_output_file_path, 'a') as f:
#                     f.write(f"{output_text} \n\n")

