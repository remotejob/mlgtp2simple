# https://snappishproductions.com/blog/2020/03/01/chapter-9.5-text-generation-with-gpt-2-and-only-pytorch.html.html

import os

os.system('pip install -U fire')

os.mkdir('trained_models')

os.system('pip install -U wandb')

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')
import wandb
wandb.init(project="gpt2model")

import numpy as np
import pyarrow.parquet as pq
import pandas as pd
import random
import torch
import fire
import logging

import csv
import json
import sqlite3

import configparser
config = configparser.ConfigParser()
config.read('/kaggle/input/mlgtp2simpledata/config.ini')
dburl = config['sqlite']['url']
limitrec = config['sqlite']['limitrec']

best_model_path ='/kaggle/working/best_model'
os.mkdir(best_model_path)

data = []
data.append({"epoch":0,"avg_val_accuracy":0.0})
with open(best_model_path +'/best_result.json', 'w') as outfile:
    json.dump(data, outfile)


from torch.utils.data import Dataset, DataLoader
from transformers import GPT2Tokenizer, GPT2LMHeadModel, AdamW, get_linear_schedule_with_warmup
from tqdm import tqdm, trange
import torch.nn.functional as F

# gpt2_type = "/kaggle/input/gpt2medium-pytorch/gpt2_medium"
gpt2_type ="gpt2"


try:
    cacheurl0 = 'file:'+dburl + '?mode=rwc&cache=shared'
    sqliteConnection = sqlite3.connect(cacheurl0, uri=True)
    cursor = sqliteConnection.cursor()

    sqlite_select_Query = "select sqlite_version();"
    cursor.execute(sqlite_select_Query)
    record = cursor.fetchall()
    cursor.close()

except sqlite3.Error as error:
    print("Error while connecting to sqlite", error)

prifixes = []

cursor = sqliteConnection.execute(
    "SELECT phrase from phrases ORDER BY RANDOM() limit " + limitrec)
for row in cursor:
    # print(row[0])
    prifixes.append(str(row[0]))

sqliteConnection.close()


class ParquetDataset(Dataset):
    def __init__(self, path, cols, truncate=False, gpt2_type=gpt2_type, max_length=768):

        self.tokenizer = GPT2Tokenizer.from_pretrained(gpt2_type)
        
        self.df = pq.read_table(path, columns=cols).to_pandas().dropna()
        for col in cols:
            self.df[col] = self.df[col].apply(lambda x: torch.tensor(self.tokenizer.encode(f"<#{col}#>{x[:768]}<|endoftext|>")))
        self.df = pd.concat(map(self.df.get, cols)).reset_index(drop=True)
        if truncate:
            self.df = self.df.truncate(after=150)

    def __len__(self):
        return self.df.count()

    def __getitem__(self, item):
        return self.df.iloc[item]

class CSVTwitter(Dataset):
    
    def __init__(self, control_code, truncate=False, gpt2_type=gpt2_type, max_length=768):

        self.tokenizer = GPT2Tokenizer.from_pretrained(gpt2_type)
        self.tweets = []
        
        with open('/kaggle/input/filteredtwdata/train.csv', newline='') as csvfile:
            tweet_csv = csv.reader(csvfile)
            for row in tweet_csv:
                self.tweets.append(torch.tensor(
                    # self.tokenizer.encode(f"<|{control_code}|>{row[0][:max_length]}<|endoftext|>")
                    self.tokenizer.encode(f"{row[0][:max_length]}<|endoftext|>")
                ))
                
        if truncate:
            self.tweets = self.tweets[:10000]
        self.tweet_count = len(self.tweets)
        
    def __len__(self):
        return self.tweet_count

    def __getitem__(self, item):
        return self.tweets[item]
 

def pack_tensor(new_tensor, packed_tensor, max_seq_len):
    if packed_tensor is None:
        return new_tensor, True, None
    if new_tensor.size()[1] + packed_tensor.size()[1] > max_seq_len:
        return packed_tensor, False, new_tensor
    else:
        packed_tensor = torch.cat([new_tensor, packed_tensor[:, 1:]], dim=1)
        return packed_tensor, True, None

def train(
    dataset,
    model,
    tokenizer,
    batch_size=16,
    epochs=15,
    lr=2e-5,
    max_seq_len=400,
    warmup_steps=5000,
    gpt2_type=gpt2_type,
    device="cuda",
    output_dir=".",
    output_prefix="wreckgar",
    test_mode=False,
    save_model_on_epoch=False,
):

    print("dataset size",len(dataset),'epochs',epochs)
    print("")

    acc_steps = 100

    model = model.to(device)
    model.train()

    optimizer = AdamW(model.parameters(), lr=lr,eps = 5e-5)

    # total_steps = len(dataset) * 2
    # total_steps = len(dataset)
    total_steps = len(dataset) / 250
    scheduler = get_linear_schedule_with_warmup(
        # optimizer, num_warmup_steps=warmup_steps, num_training_steps=-1
        optimizer, num_warmup_steps=warmup_steps, num_training_steps=total_steps
    )

    train_dataloader = DataLoader(dataset, batch_size=1, shuffle=False)

    accumulating_batch_count = 0
    input_tensor = None

    best_train_loss = 100.0
    for epoch in range(epochs):

        print(f"Training epoch {epoch}")
        for idx, entry in tqdm(enumerate(train_dataloader)):
            (input_tensor, carry_on, remainder) = pack_tensor(entry, input_tensor, 768)

            if carry_on and idx != len(train_dataloader) - 1:
                continue

            input_tensor = input_tensor.to(device)
            outputs = model(input_tensor, labels=input_tensor)
            loss = outputs[0]
            wandb.log({"train_loss": loss.item()})
            if best_train_loss > loss.item():
                print("Save BEST!! epoch",epoch,"idx",idx,"was",best_train_loss,"now",loss.item())
                model.save_pretrained(best_model_path)
                best_train_loss = loss.item()

            loss.backward()

            if (accumulating_batch_count % batch_size) == 0:
                optimizer.step()
                lr = scheduler.get_last_lr()[0]
                wandb.log({'lr':lr})
                scheduler.step()
                optimizer.zero_grad()
                model.zero_grad()

            accumulating_batch_count += 1
            input_tensor = None
        if save_model_on_epoch:
            torch.save(
                model.state_dict(),
                os.path.join(output_dir, f"{output_prefix}-{epoch}.pt"),
            )
    return model

dataset = CSVTwitter("<|Hei|>", truncate=False, gpt2_type=gpt2_type)

model = train(
    dataset,
    GPT2LMHeadModel.from_pretrained(gpt2_type),
    GPT2Tokenizer.from_pretrained(gpt2_type),
    batch_size=128,
    epochs=10,#9
    lr=1e-5,
    max_seq_len=140,
    warmup_steps=200,
    gpt2_type=gpt2_type,
    device="cuda",
    output_dir="trained_models",
    output_prefix="twitter",
    save_model_on_epoch=False
)

def generate(
    model,
    tokenizer,
    prompt,
    entry_count=19,
    entry_length=100,
    top_p=0.8,
    temperature=1.,
):

    model.eval()

    generated_num = 0
    generated_list = []

    filter_value = -float("Inf")

    with torch.no_grad():

        for entry_idx in trange(entry_count):

            entry_finished = False

            generated = torch.tensor(tokenizer.encode(prompt[entry_idx].capitalize())).unsqueeze(0)

            for i in range(entry_length):
                outputs = model(generated, labels=generated)
                loss, logits = outputs[:2]
                logits = logits[:, -1, :] / (temperature if temperature > 0 else 1.0)

                sorted_logits, sorted_indices = torch.sort(logits, descending=True)
                cumulative_probs = torch.cumsum(
                    F.softmax(sorted_logits, dim=-1), dim=-1
                )

                sorted_indices_to_remove = cumulative_probs > top_p
                sorted_indices_to_remove[..., 1:] = sorted_indices_to_remove[
                    ..., :-1
                ].clone()
                sorted_indices_to_remove[..., 0] = 0

                indices_to_remove = sorted_indices[sorted_indices_to_remove]
                logits[:, indices_to_remove] = filter_value

                next_token = torch.multinomial(F.softmax(logits, dim=-1), num_samples=1)
                generated = torch.cat((generated, next_token), dim=1)

                if next_token in tokenizer.encode("<|endoftext|>"):
                    entry_finished = True

                if entry_finished:

                    generated_num = generated_num + 1

                    output_list = list(generated.squeeze().numpy())
                    output_text = tokenizer.decode(output_list)

                    generated_list.append(output_text)
                    break
            
            if not entry_finished:
                output_list = list(generated.squeeze().numpy())
                output_text = f"{tokenizer.decode(output_list)}<|endoftext|>" 
                generated_list.append(output_text)
                
    return generated_list


model = GPT2LMHeadModel.from_pretrained(best_model_path)
generated_tweets = generate(model.to('cpu'), GPT2Tokenizer.from_pretrained(gpt2_type),prifixes,entry_count=19)


print(generated_tweets)

with open('result.txt', 'w') as filehandle:
    for listitem in generated_tweets:
        clean = listitem.replace('<|endoftext|>','')
        print(clean) 
        filehandle.write('%s\n' % clean)


