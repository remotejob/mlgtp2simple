import os
os.system('pip install --no-cache-dir -U tensorflow-gpu==1.15.0')
os.system('pip install --no-cache-dir -U gpt_2_simple')
os.system('pip install --no-cache-dir -U configparser')

import tensorflow as tf
import gpt_2_simple as gpt2
import sqlite3
import configparser
import requests

config = configparser.ConfigParser()
config.read('/kaggle/input/mlgtp2simpledata/config.ini')
dburl = config['sqlite']['url']
limitrec = config['sqlite']['limitrec']

sess = gpt2.start_tf_sess()
gpt2.load_gpt2(
    sess, checkpoint_dir='/kaggle/input/mlgtp2simpledata/checkpoint')

generate_count = 0

while True:

    try:
        cacheurl0 = 'file:'+dburl + '?mode=rwc&cache=shared'
        sqliteConnection = sqlite3.connect(cacheurl0, uri=True)
        cursor = sqliteConnection.cursor()
        #print("Database created and Successfully Connected to SQLite")

        sqlite_select_Query = "select sqlite_version();"
        cursor.execute(sqlite_select_Query)
        record = cursor.fetchall()
        #print("SQLite Database Version is: ", record)
        cursor.close()

    except sqlite3.Error as error:
        print("Error while connecting to sqlite", error)

    prifixes = []

    cursor = sqliteConnection.execute(
        "SELECT phrase from phrases ORDER BY RANDOM() limit " + limitrec)
    for row in cursor:
        # print(row[0])
        prifixes.append(str(row[0]))

    sqliteConnection.close()

    for x in prifixes:
        # print(x)
        if x[:1] != 'ä':
            pr = x.capitalize()
        else:
            pr = x
        # print('-----------------------')
        prarr = pr.split()
        pr = ' '.join(prarr)
        single_text = gpt2.generate(
            sess, prefix=pr, include_prefix=False, return_as_list=True, checkpoint_dir='/kaggle/input/mlgtp2simpledata/checkpoint')[0]
        toinsert = [x, single_text]

        bestend = single_text.find('<|endoftext|>')
        if bestend > 0:
            # print(single_text[:bestend])
            bestphase = single_text[:bestend]
            if len(bestphase) == len(pr):
                # print("Best:",bestphase)
                bestphase = ""
        else:
            bestphase = ""

        if bestphase != "":
            bestphasearr = bestphase.split()
            bestphase = ' '.join(bestphasearr)

        pload = {'Prompt': x, 'Bestphrase': bestphase, 'Orgtxt': single_text}

        r = requests.post('http://167.172.230.166:6000/v0/insertque', json=pload)
        r.close()

    generate_count += 1
    if generate_count == 5:
        print(generate_count)

        sess = gpt2.reset_session(sess)
        gpt2.load_gpt2(
            sess, checkpoint_dir='/kaggle/input/mlgtp2simpledata/checkpoint')
        generate_count = 0
