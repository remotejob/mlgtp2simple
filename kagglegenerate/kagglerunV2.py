
import numpy as np
import pandas as pd
import random
import torch
import logging

from tqdm import tqdm, trange
import torch.nn.functional as F

# import json
import sqlite3

from transformers import GPT2Tokenizer, GPT2LMHeadModel

import requests
import configparser

gpt2_type ="gpt2"

config = configparser.ConfigParser()
config.read('/kaggle/input/mlgtp2simpledata/config.ini')
dburl = config['sqlite']['url']
# limitrec = config['sqlite']['limitrec']

device = 'cpu'
if torch.cuda.is_available():
    device = 'cuda'

# generate_count = 0

def generate(
    model,
    tokenizer,
    prompt,
    device='cpu',
    entry_count=19,
    entry_length=100,
    top_p=0.8,
    temperature=1.,
):

    model.to(device)
    model.eval()

    generated_num = 0
    generated_list = []

    filter_value = -float("Inf")

    with torch.no_grad():

        for entry_idx in trange(entry_count):

            entry_finished = False

            generated = torch.tensor(tokenizer.encode(prompt[entry_idx].capitalize())).to(device).unsqueeze(0)
            # generated.to(device)

            for i in range(entry_length):
                outputs = model(generated, labels=generated)
                loss, logits = outputs[:2]
                logits = logits[:, -1, :] / (temperature if temperature > 0 else 1.0)

                sorted_logits, sorted_indices = torch.sort(logits, descending=True)
                cumulative_probs = torch.cumsum(
                    F.softmax(sorted_logits, dim=-1), dim=-1
                )

                sorted_indices_to_remove = cumulative_probs > top_p
                sorted_indices_to_remove[..., 1:] = sorted_indices_to_remove[
                    ..., :-1
                ].clone()
                sorted_indices_to_remove[..., 0] = 0

                indices_to_remove = sorted_indices[sorted_indices_to_remove]
                logits[:, indices_to_remove] = filter_value

                next_token = torch.multinomial(F.softmax(logits, dim=-1), num_samples=1)
                generated = torch.cat((generated, next_token), dim=1)

                if next_token in tokenizer.encode("<|endoftext|>"):
                    entry_finished = True

                if entry_finished:

                    generated_num = generated_num + 1

                    output_list = list(generated.cpu().squeeze().numpy())
                    output_text = tokenizer.decode(output_list)

                    generated_list.append("<|startoftext|>"+ output_text)
                    break
            
            if not entry_finished:
                output_list = list(generated.cpu().squeeze().numpy())
                output_text = f"{tokenizer.decode(output_list)}<|endoftext|>" 
                generated_list.append("<|startoftext|>"+output_text)
                
    return generated_list


model = GPT2LMHeadModel.from_pretrained('/kaggle/input/mlgtp2simpledata/models')
tokenizer = GPT2Tokenizer.from_pretrained(gpt2_type)

while True:  

    limitrec = random.randint(10, 15)
   
    try:
        cacheurl0 = 'file:'+dburl + '?mode=rwc&cache=shared'
        sqliteConnection = sqlite3.connect(cacheurl0, uri=True)
        cursor = sqliteConnection.cursor()
        #print("Database created and Successfully Connected to SQLite")

        sqlite_select_Query = "select sqlite_version();"
        cursor.execute(sqlite_select_Query)
        record = cursor.fetchall()
        cursor.close()

    except sqlite3.Error as error:
        print("Error while connecting to sqlite", error)

    prifixes = []

    cursor = sqliteConnection.execute(
        "SELECT phrase from phrases ORDER BY RANDOM() limit " + str(limitrec))
    for row in cursor:
        prifixes.append(str(row[0]))

    sqliteConnection.close()

    generated_tweets = generate(model,tokenizer,prifixes,device,entry_count=limitrec)

    count = 0
    bestphase = ""
    single_text =""
    for twits in generated_tweets:
        if count == 0:
            bestphase = twits.replace('<|startoftext|>','').replace('<|endoftext|>','')
            single_text += twits.replace('<|startoftext|>','',1)
        else:
            # print(count,twits)
            single_text += "\n"+twits

        count += 1

    pload = {'Prompt': prifixes[0], 'Bestphrase': bestphase, 'Orgtxt': single_text}

    # print(pload)

    r = requests.post('http://167.172.230.166:6000/v0/insertque', json=pload)
    r.close()
