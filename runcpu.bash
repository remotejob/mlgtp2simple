export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp kernelcpu/kernel-metadata.json.remotejob kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip
cp kernelcpu/kernel-metadata.json.sipvip kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp kernelcpu/kernel-metadata.json.almazseo kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kaggleipotecafi
cp kernelcpu/kernel-metadata.json.ipotecafi kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglekagkagdevprod
cp kernelcpu/kernel-metadata.json.kagkagdevprod kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglesupportsupport
cp kernelcpu/kernel-metadata.json.supportsupport kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp kernelcpu/kernel-metadata.json.almazurov kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/


unset KAGGLE_CONFIG_DIR
cp kernelcpu/kernel-metadata.json.alesandermazurov kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/