export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp kernelgpu/kernel-metadata.json.remotejob kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglesipvip
cp kernelgpu/kernel-metadata.json.sipvip kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp kernelgpu/kernel-metadata.json.almazseo kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kaggleipotecafi
cp kernelgpu/kernel-metadata.json.ipotecafi kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglekagkagdevprod
cp kernelgpu/kernel-metadata.json.kagkagdevprod kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglesupportsupport
cp kernelgpu/kernel-metadata.json.supportsupport kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp kernelgpu/kernel-metadata.json.almazurov kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

unset KAGGLE_CONFIG_DIR
cp kernelgpu/kernel-metadata.json.alesandermazurov kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/


